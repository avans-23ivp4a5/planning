package datastorage;

import domain.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Robin
 */
public class AvailabilityDAO {
DatabaseConnection connection = new DatabaseConnection();
    public AvailabilityDAO() {
        connection.openConnection();
        // Nothing to be initialized. This is a stateless class. Constructor
        // has been added to explicitely make this clear.
    }

    /**
     * @param date
     * @param shift
     * @param employeeId
     * @return the Order object to be found. In case order could not be found,
     * null is returned.
     */
    public Availability find(String date, String shift, int employeeId) {
        Availability availability = null;

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `availability` WHERE `date` = '" + date + "' AND `shift` = '" + shift + "'  AND `employeeId` = " + employeeId + ";");

            if (resultset != null) {
                try {
                    // The ordershipnumber for a order is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if (resultset.next()) {
                        String dbDate = resultset.getString("date");
                        String dbShift = resultset.getString("shift");
                        int dbEmployeeId = resultset.getInt("employeeId");
                        String dbCheckInTime = resultset.getString("checkInTime");
                        String dbCheckIOutTime = resultset.getString("checkOutTime");
                        boolean dbScheduled = resultset.getBoolean("scheduled");

                        availability = new Availability(
                                dbDate,
                                dbShift,
                                dbEmployeeId,
                                dbCheckInTime,
                                dbCheckIOutTime,
                                dbScheduled
                        );
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    availability = null;
                }
            }
            // else an error occurred leave 'order' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return availability;
    }

    public boolean insert(Availability availability) {
        boolean result = false;

        if (availability != null) {
            String checkOutTime = null;
            String checkInTime = null;

            // check in time
            if (availability.getCheckInTime() != null) {
                checkInTime = "'" + availability.getCheckInTime() + "'";
            }

            // check out time
            if (availability.getCheckOutTime() != null) {
                checkOutTime = "'" + availability.getCheckOutTime() + "'";
            }

            DatabaseConnection connection = new DatabaseConnection();
            if (connection.openConnection()) {
                connection.executeSQLInsertStatement(
                        "INSERT INTO `availability` SET "
                        + "`date` = '" + availability.getDate() + "', "
                        + "`shift` = '" + availability.getShift() + "',"
                        + "`employeeId` = " + availability.getEmployeeId() + ","
                        + "`checkInTime` = " + checkInTime + ","
                        + "`checkOutTime` = " + checkOutTime + ","
                        + "`scheduled` = " + availability.isScheduled() + ""
                        + ";"
                );

                // Finished with the connection, so close it.
                connection.closeConnection();
            }
        }

        return result;
    }

    public boolean update(Availability availability) {
        boolean result = false;

        if (availability != null) {
            DatabaseConnection connection = new DatabaseConnection();
            if (connection.openConnection()) {
                connection.executeSQLInsertStatement(
                        "UPDATE `availability` "
                        + " SET "
                        + "`checkInTime` = '" + availability.getCheckInTime() + "',"
                        + "`checkOutTime` = '" + availability.getCheckOutTime() + "',"
                        + "`scheduled` = " + availability.isScheduled() + ""
                        + " WHERE "
                        + "`date` = '" + availability.getDate() + "' AND "
                        + "`shift` = '" + availability.getShift() + "' AND "
                        + "`employeeId` = " + availability.getEmployeeId()
                        + ";"
                );

                // Finished with the connection, so close it.
                connection.closeConnection();
            }
        }

        return result;
    }

    /**
     * Removes the given order from the database.
     *
     * @param date
     * @param shift
     * @param employeeId
     * @return true if execution of the SQL-statement was successful, false
     * otherwise.
     */
    public boolean remove(String date, String shift, int employeeId) {
        boolean result = false;

        // First open the database connection.
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // Execute the delete statement using the ordership number to
            // identify the order row.
            result = connection.executeSQLDeleteStatement(
                    "DELETE FROM `availability` WHERE "
                    + "`date` = '" + date + "' AND "
                    + "`shift` = '" + shift + "' AND "
                    + "`employeeId` = " + employeeId
                    + ";");

            // Finished with the connection, so close it.
            connection.closeConnection();
        }

        return result;
    }

    public int countShifts(int employeeId, String date) {
        int amount = 0;

        // First open the database connection.
        DatabaseConnection connection = new DatabaseConnection();

        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT COUNT(`shift`) as `amount` FROM `availability` WHERE "
                    + "`date` = '" + date + "' AND "
                    + "`employeeId` = " + employeeId
                    + ";");

            if (null != resultset) {
                try {
                    if (resultset.next()) {
                        amount = resultset.getInt("amount");
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }

            // Finished with the connection, so close it.
            connection.closeConnection();
        }

        return amount;
    }

    public void removeShiftsOnDay(int employeeId, String date) {
        remove(date, "Ochtend", employeeId);
        remove(date, "Middag", employeeId);
        remove(date, "Avond", employeeId);
    }

    public ArrayList getPresents() throws ParseException {

//welkeshif-begin
        String shift = "Geen";
        int time;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        time = Integer.parseInt(sdf.format(cal.getTime()).toString());

        if (9 <= time && 13 >= time) {
            shift = "Ochtend";
        }

        if (13 <= time && 17 >= time) {
            shift = "Middag";
        }

        if (17 <= time && 21 >= time) {
            shift = "Avond";
        }
//welkeshifteind  

        ArrayList<Employee> employees = new ArrayList<>();

        String today;
        Calendar cal2 = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        today = (sdf2.format(cal2.getTime()));

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT employee.* FROM employee, availability WHERE date='" + today + "' AND checkInTime IS NOT null AND checkOutTime IS null AND shift = '" + shift + "' AND availability.employeeId = employee.id;");

            if (resultset != null) {
                try {
                    // The orderLineshipnumber for a orderLine is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        employees.add(new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        ));
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    employees = null;
                }
            }
            // else an error occurred leave 'orderLine' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return employees;
    }

    public ArrayList getNotPresents() throws ParseException {

        //welkeshif-begin
        String shift = "Geen";
        int time;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        time = Integer.parseInt(sdf.format(cal.getTime()).toString());

        if (9 <= time && 13 >= time) {
            shift = "Ochtend";
        }

        if (13 <= time && 17 >= time) {
            shift = "Middag";
        }

        if (17 <= time && 21 >= time) {
            shift = "Avond";
        }
        //welkeshifteind  

        ArrayList<Employee> employees = new ArrayList<>();

        String today;
        Calendar cal2 = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        today = (sdf2.format(cal2.getTime()));

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT employee.* FROM employee, availability WHERE  checkInTime IS null AND checkOutTime IS null AND shift = '" + shift + "' AND date ='" + today + "' AND scheduled = 1 AND availability.employeeId = employee.id;");

            if (resultset != null) {
                try {
                    // The orderLineshipnumber for a orderLine is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        employees.add(new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        ));
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    employees = null;
                }
            }
            // else an error occurred leave 'orderLine' to null.
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return employees;
    }
    

    public HashMap getWorkedHoursMonth() {
        HashMap<Integer, String> workedHours = new HashMap<>();

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT employeeId, SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(checkOutTime,checkInTime)))) AS'hoursWorkedMonth' FROM `availability` WHERE scheduled = true AND date BETWEEN ADDDATE(LAST_DAY(ADDDATE(NOW(), INTERVAL -2 MONTH)), 1)   AND Last_DAY(DATE_ADD(NOW(), INTERVAL -1 MONTH)) GROUP BY employeeId;");

            if (resultset != null) {
                try {
                    // The ordershipnumber for a order is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        int dbId = resultset.getInt("employeeId");
                        String dbHoursWorkedMonth = resultset.getString("hoursWorkedMonth");

                        workedHours.put(dbId, dbHoursWorkedMonth);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    workedHours = null;
                }
            }
            // else an error occurred leave 'order' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return workedHours;
    }
    
    public ArrayList<Employee> search(String date, int functionId, String shift) {
        ArrayList<Employee> employees = new ArrayList<>();
        
        if (!connection.connectionIsOpen()) {
            connection.openConnection();
        }
        
        // If a connection was successfully setup, execute the SELECT statement.
        ResultSet resultset = connection.executeSQLSelectStatement(
                "SELECT `employee`.*, `availability`.`shift`, `availability`.`employeeId`, `availability`.`scheduled` FROM `availability` JOIN `employee` ON `availability`.`employeeId` = `employee`.`id` JOIN `function` ON `employee`.`functionId` = `function`.`id` WHERE `date` = '"+ date +"' AND `functionId` = "+ functionId +" AND `shift` = '"+ shift +"' ORDER BY `availability`.`scheduled` DESC");

        if (resultset != null) {
            try {
                // The ordershipnumber for a order is unique, so in case the
                // resultset does contain data, we need its first entry.
                while (resultset.next()) {                        
                    // employee
                    int id = resultset.getInt("id");
                    String firstName = resultset.getString("firstName");
                    String infixName = resultset.getString("infixName");
                    String lastName = resultset.getString("lastName");

                    // init employee object
                    Employee employee = new Employee(
                        id,
                        firstName,
                        infixName,
                        lastName,
                        functionId
                    );
                    
                    if (resultset.getBoolean("scheduled")) {
                        employee.setIsScheduled();
                    }
                    
                    employees.add(employee);
                }
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        
        return employees;
    }
    
    
    
    public ArrayList getScheduled(String shift, String date, int who) throws ParseException {
        ArrayList<Employee> employees = new ArrayList<>();

        // First open a database connnection
        
        if (!connection.connectionIsOpen()) {
            connection.openConnection();
        }

  
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset;
            if(who == 0){
            resultset = connection.executeSQLSelectStatement(
                    "SELECT employee.* FROM employee, availability WHERE date = '"+date+"' AND shift = '"+shift+"' AND scheduled = 1 AND availability.employeeId = employee.id ORDER BY employee.functionId;");
            }else{
                resultset = connection.executeSQLSelectStatement(
                    "SELECT employee.* FROM employee, availability WHERE employee.id = '"+who+"' AND date = '"+date+"' AND shift = '"+shift+"' AND scheduled = 1 AND availability.employeeId = employee.id ORDER BY employee.functionId;");
            }
            if (resultset != null) {
                try {
                    // The orderLineshipnumber for a orderLine is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        employees.add(new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        ));
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    employees = null;
                }
            }
            
            // else an error occurred leave 'orderLine' to null.
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            //connection.closeConnection();
        
    
        return employees;
    }
    
    public void updateScheduled(String selectedDate, String shift, int employeeId, boolean scheduled){        
        // open connection if closed
        if (!connection.connectionIsOpen()){
            connection.openConnection();
        }
        
        // If a connection was successfully setup, execute the SELECT statement.
        connection.executeSQLInsertStatement("UPDATE availability SET scheduled=" + scheduled + " WHERE employeeId=" + employeeId + " AND date='" + selectedDate + "' AND shift='" + shift + "';");
    }
}
