/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastorage;

import domain.Availability;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import domain.Employee;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Jari
 */
public class EmployeeDAO {

    public ArrayList findEmployees() {
        ArrayList<Employee> employees = new ArrayList<>();

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM employee");

            if (resultset != null) {
                try {
                    // The orderLineshipnumber for a orderLine is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        employees.add(new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        ));
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    employees = null;
                }
            }
            // else an error occurred leave 'orderLine' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        return employees;
    }

    public void insertEmployee(String username,String password,String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, int active, String functionId) {
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.

            connection.executeSQLInsertStatement("INSERT INTO employee (username, password, firstName, infixName, lastName, dateOfBirth, bankAccount, hourlyWage, monthlyHours, address, postalCode, email, phoneNo, joinDate, active, functionId)"
                    + "VALUES (" + getValue(username) + "," + getValue(password) + "," + getValue(firstName) + "," + getValue(infixName) + "," + getValue(lastName) + "," + getValue(dateOfBirth) + "," + getValue(accountNumber) + ", '" + hourlyWage + "', '" + monthlyHours + "'," + getValue(address) + "," + getValue(postalCode) + "," + getValue(email) + "," + getValue(phoneNo) + "," + getValue(joinDate) + ",'" + active + "'," + getValue(functionId) + ")");

            // else an error occurred leave 'order' to null.
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

    }
    
    private String getValue(String value) {
        if (value.equals("")) {
            return null;
        }
        
        return "'" + value + "'";
    }

    public void updateEmployee(int employeeId,String username,String password,String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, int active, String functionId) {
        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
                
            connection.executeSQLInsertStatement("UPDATE employee SET userName = " + getValue(username) + ",password = " + getValue(password) + ",firstName = " + getValue(firstName) + ", infixName = " + getValue(infixName) + ", lastName=" + getValue(lastName) + ", dateOfBirth=" + getValue(dateOfBirth) + ", BankAccount=" + getValue(accountNumber) + ", hourlyWage='" + hourlyWage + "', monthlyHours='" + monthlyHours + "', address=" + getValue(address) + ", postalCode=" + getValue(postalCode) + ", email=" + getValue(email) + ", phoneNo=" + getValue(phoneNo) + ", joinDate=" + getValue(joinDate) + ", active='" + active + "', functionId=" + getValue(functionId) + " WHERE id = '" + employeeId + "'"
            );

            // else an error occurred leave 'order' to null.
            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

    }

    public Employee find(int employee_id) {
        Employee changer = null;

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `employee` WHERE `id` = " + employee_id + " ;");

            if (resultset != null) {
                try {
                    // The ordershipnumber for a order is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if (resultset.next()) {

                        changer = new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        );
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    changer = null;
                }
            }
            // else an error occurred leave 'order' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return changer;
    }

    public Employee checkUsername(String username) {
               Employee user = null;

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `employee` WHERE `userName` = '" + username + "' ;");

            if (resultset != null) {
                try {
                    // The ordershipnumber for a order is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if (resultset.next()) {

                        user = new Employee(
                                resultset.getInt("id"),
                                resultset.getString("username"),
                                resultset.getString("password"),
                                resultset.getString("firstName"),
                                resultset.getString("infixName"),
                                resultset.getString("lastName"),
                                resultset.getString("dateOfBirth"),
                                resultset.getString("bankAccount"),
                                resultset.getInt("hourlyWage"),
                                resultset.getInt("monthlyHours"),
                                resultset.getString("address"),
                                resultset.getString("postalCode"),
                                resultset.getString("email"),
                                resultset.getString("phoneNo"),
                                resultset.getString("joinDate"),
                                resultset.getBoolean("active"),
                                resultset.getInt("functionId")
                        );
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    user = null;
                }
            }
            // else an error occurred leave 'order' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return user;

}}
