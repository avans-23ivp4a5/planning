/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastorage;

import domain.Employee;
import domain.Function;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Chiel
 */
public class FunctionDAO {
    
    public ArrayList<Function> findFunctions() {
        ArrayList<Function> functions = new ArrayList<>();

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM function;");

            if (resultset != null) {
                try {
                    // The orderLineshipnumber for a orderLine is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    while (resultset.next()) {
                        functions.add(new Function(
                                resultset.getInt("id"),
                                resultset.getString("functionName")
                        ));
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    functions = null;
                }
            }
            // else an error occurred leave 'orderLine' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }
        return functions;
    }
    
}
