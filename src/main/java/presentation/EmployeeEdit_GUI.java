/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import javax.swing.JPanel;
import businesslogic.*;
import com.toedter.calendar.JDateChooser;
import domain.Employee;
import domain.Function;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Chiel
 */
public class EmployeeEdit_GUI extends JFrame {

    EmployeeEdit_Panel employeeEditPanel;

    public EmployeeEdit_GUI(int id, EmployeeManager employeeManager, FunctionManager functionManager) throws ParseException {

        employeeEditPanel = new EmployeeEdit_Panel(this, id, employeeManager, functionManager);

        setSize(500, 700);
        setTitle("Hartige Hap - Planning - Personeelsbeheer - Wijzigen");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setContentPane(employeeEditPanel);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    class EmployeeEdit_Panel extends JPanel {

        EmployeeEdit_GUI window;
        EmployeeManager employeeManager;
        FunctionManager functionManager;

        SelectItem selectedItemObject;

        JComboBox functionList;

        String firstname, infixname, lastname, email, phonenumber, birthday, joindate, function, username, password, accountnumber, adress, postalcode;
        int hourlywage, monthlyhours, id;
        boolean active;
        SimpleDateFormat dateFormattrue = new SimpleDateFormat("yyyy-MM-dd");

        public EmployeeEdit_Panel(EmployeeEdit_GUI window, int id, EmployeeManager employeeManager, FunctionManager functionManger) throws ParseException {
            this.setBorder(new EmptyBorder(10, 10, 10, 10));
            this.employeeManager = employeeManager;
            this.window = window;
            this.id = id;
            this.functionManager = functionManger;

            Employee changer = employeeManager.getEmployeeFromDB(id);

            String firstnameIN = changer.getFirstName();
            String infixnameIN = changer.getInfixName();
            String lastnameIN = changer.getLastName();
            Date borndateIN = employeeManager.convertdate(changer.getDateOfBirth());
            String emailIN = changer.getEmail();
            String phonenumberIN = changer.getPhoneNo();
            String accountnumberIN = changer.getAccountNumber();
            String adressIN = changer.getAddress();
            String postalcodeIN = changer.getPostalCode();
            Date joindateIN = employeeManager.convertdate(changer.getDateOfBirth());
            int hourlywageIN = changer.getHourlyWage();
            int monthlyhoursIN = changer.getMonthlyHours();
            int functionIN = changer.getFunctionId();
            boolean activeIN = changer.isActive();
            String usernameIN = changer.getUsername();
            String passwordIN = changer.getPassword();

            setLayout(new GridLayout(17, 4, 4, 4));

            JLabel firstnameLabel = new JLabel("Voornaam*");
            JTextField firstnameField = new JTextField();
            add(firstnameLabel);
            add(firstnameField);
            firstnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            firstnameField.setText(firstnameIN);

            JLabel infixnameLabel = new JLabel("Tussenvoegsel");
            JTextField infixnameField = new JTextField();
            add(infixnameLabel);
            add(infixnameField);
            infixnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            infixnameField.setText(infixnameIN);

            JLabel lastnameLabel = new JLabel("Achternaam*");
            JTextField lastnameField = new JTextField();
            add(lastnameLabel);
            add(lastnameField);
            lastnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            lastnameField.setText(lastnameIN);

            JLabel birthdayLabel = new JLabel("Geboortedatum*");
            JDateChooser birthdayChooser = new JDateChooser(borndateIN);
            birthdayChooser.getDateEditor().setEnabled(false);
            add(birthdayLabel);
            add(birthdayChooser);

            JLabel emailLabel = new JLabel("E-mail");
            JTextField emailField = new JTextField();
            add(emailLabel);
            add(emailField);
            emailField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            emailField.setText(emailIN);

            JLabel phonenumberLabel = new JLabel("Telefoonnummer");
            JTextField phonenumberField = new JTextField();
            add(phonenumberLabel);
            add(phonenumberField);
            phonenumberField.setDocument(new EmployeeManagement_JTextFieldLimit(14));
            phonenumberField.setText(phonenumberIN);

            JLabel accountnumberLabel = new JLabel("Rekeningnummer");
            JTextField accountnumberField = new JTextField();
            add(accountnumberLabel);
            add(accountnumberField);
            accountnumberField.setDocument(new EmployeeManagement_JTextFieldLimit(21));
            accountnumberField.setText(accountnumberIN);

            JLabel adressLabel = new JLabel("Straat + huisummer");
            JTextField adressField = new JTextField();
            add(adressLabel);
            add(adressField);
            adressField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            adressField.setText(adressIN);

            JLabel postalCodeLabel = new JLabel("Postcode");
            JTextField postalCodeField = new JTextField();
            add(postalCodeLabel);
            add(postalCodeField);
            postalCodeField.setDocument(new EmployeeManagement_JTextFieldLimit(7));
            postalCodeField.setText(postalcodeIN);

            JLabel joinDateLabel = new JLabel("Datum in dienst treden");
            JDateChooser joinDateChooser = new JDateChooser(joindateIN);
            joinDateChooser.getDateEditor().setEnabled(false);
            add(joinDateLabel);
            add(joinDateChooser);

            JLabel hourlywageLabel = new JLabel("Uurloon");
            SpinnerModel sm1 = new SpinnerNumberModel(0, 0, 200, 1);
            JSpinner hourlywageSpinner = new JSpinner(sm1);
            hourlywageSpinner.setValue(new Integer(hourlywageIN));
            add(hourlywageLabel);
            add(hourlywageSpinner);

            JLabel monthlyhoursLabel = new JLabel("Contracturen");
            SpinnerModel sm2 = new SpinnerNumberModel(0, 0, 50, 1);
            JSpinner monthlyhoursSpinner = new JSpinner(sm2);
            monthlyhoursSpinner.setValue(new Integer(monthlyhoursIN));
            add(monthlyhoursLabel);
            add(monthlyhoursSpinner);

            JLabel functionLabel = new JLabel("Functie");
            Vector model = new Vector();
            ArrayList<Function> functionsArrayList = functionManager.getFunctions();
            functionsArrayList.forEach((Function function) -> {
                SelectItem selectedItem = new SelectItem(function.getFunctionId(), function.getFunctionName());
                if (function.getFunctionId() == functionIN) {
                    selectedItemObject = selectedItem;
                }
                model.addElement(selectedItem);
            });
            functionList = new JComboBox(model);
            functionList.setSelectedItem(selectedItemObject);
            add(functionLabel);
            add(functionList);

            JLabel activLabel = new JLabel("Actief");
            JCheckBox activBox = new JCheckBox();
            activBox.setSelected(activeIN);
            add(activLabel);
            add(activBox);

            JLabel usernameLabel = new JLabel("Gebruikersnaam");
            JTextField usernameField = new JTextField();
            add(usernameLabel);
            add(usernameField);
            usernameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            usernameField.setText(usernameIN);

            JLabel passwordLabel = new JLabel("Wachtwoord");
            JPasswordField passwordField = new JPasswordField();
            add(passwordLabel);
            add(passwordField);
            passwordField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            passwordField.setText(passwordIN);

            JLabel saveLabel = new JLabel("");
            JButton saveButton = new JButton("Opslaan");
            add(saveLabel);
            add(saveButton);

            saveButton.addActionListener((ActionEvent e) -> {
                username = usernameField.getText();
                password = passwordField.getText();
                firstname = firstnameField.getText();
                infixname = infixnameField.getText();
                lastname = lastnameField.getText();
                accountnumber = accountnumberField.getText();
                email = emailField.getText();
                phonenumber = phonenumberField.getText();
                adress = adressField.getText();
                postalcode = postalCodeField.getText();

                Date birthdaycheck = birthdayChooser.getDate();

                if (birthdaycheck == null) {
                    birthday = "";
                } else {
                    birthday = dateFormattrue.format(birthdayChooser.getDate());
                }

                Date jooindatecheck = joinDateChooser.getDate();

                if (jooindatecheck == null) {
                    joindate = "";
                } else {
                    joindate = dateFormattrue.format(joinDateChooser.getDate());
                }

                hourlywage = (int) hourlywageSpinner.getValue();
                monthlyhours = (int) monthlyhoursSpinner.getValue();

                function = "" + this.getFunctionId();

                active = activBox.isSelected();

                String result = employeeManager.checkInsertUpdate(changer.getEmployeeId(), username, password, firstname, infixname, lastname, birthday, accountnumber, hourlywage, monthlyhours, adress, postalcode, email, phonenumber, joindate, active, function);
                saveLabel.setForeground(Color.red);

                if (! employeeManager.checkInsertUpdate_boolean(changer.getEmployeeId(), username, password, firstname, infixname, lastname, birthday, accountnumber, hourlywage, monthlyhours, adress, postalcode, email, phonenumber, joindate, active, function)) {
                    employeeManager.refreshFrame();
                    window.dispose();
                } else {
                    saveLabel.setText(result);
                }
            });
        }

        private int getFunctionId() {
            // function
            SelectItem functionItem = (SelectItem) functionList.getSelectedItem();
            int functionId = functionItem.getId();

            return functionId;
        }

    }
}
