package presentation;

import businesslogic.AvailabilityManager;
import businesslogic.EmployeeManager;
import domain.Employee;
import java.awt.Component;
import java.awt.HeadlessException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import businesslogic.ScheduleManager;
import java.text.*;
import java.time.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Patrick
 */
public class Dashboard_GUI extends javax.swing.JFrame {

    EmployeeManager employeeManager;
    AvailabilityManager availabilityManager;
    ScheduleManager scheduleManager;

    Date mondayDate;
    Date tuesdayDate;
    Date wednesdayDate;
    Date thursdayDate;
    Date fridayDate;
    Date saturdayDate;
    Date sundayDate;
    DefaultTableModel model1, model2;
    JLabel legend;
    int who, functionId;
    Vector modelv;
    SelectItem selectedItemObject;

    private void updateRowHeights(JTable table) {
        for (int row = 0; row < table.getRowCount(); row++) {
            int rowHeight = table.getRowHeight();

            for (int column = 0; column < table.getColumnCount(); column++) {
                Component comp = table.prepareRenderer(table.getCellRenderer(row, column), row, column);
                rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
            }

            table.setRowHeight(row, rowHeight);
            table.setSize(2000, HEIGHT);
        }
    }

    private String getNames(String shift, Date date, int who) throws ParseException {
        ArrayList<String> scheduledNames = new ArrayList<>();
        ArrayList<Employee> scheduledEmployees = new ArrayList<>();
        scheduledEmployees = scheduleManager.scheduled(shift, date, who);

        // set colors
        HashMap<Integer, String> colors = new HashMap<>();
        colors.put(1, "red");
        colors.put(2, "green");
        colors.put(3, "blue");
        colors.put(4, "orange");
        colors.put(5, "blue");
        
        for (Employee employee : scheduledEmployees) {
            scheduledNames.add("<p style='color:" + colors.get(employee.getFunctionId()) + ";'>" + employee.getFullName() + "</p>");
        }

        String PersonsString = "<html>";
        for (String s : scheduledNames) {
            PersonsString += s;
        }
        PersonsString += "</html>";

        return PersonsString;
    }

    public void refreshTable() throws ParseException {
        model2 = new DefaultTableModel(
                new Object[][]{
                    {"Ochtend 09:00 - 13:00", this.getNames("Ochtend", mondayDate, who), this.getNames("Ochtend", tuesdayDate, who), this.getNames("Ochtend", wednesdayDate, who), this.getNames("Ochtend", thursdayDate, who), this.getNames("Ochtend", fridayDate, who), this.getNames("Ochtend", saturdayDate, who), this.getNames("Ochtend", sundayDate, who)},
                    {"Middag 13:00 - 17:00", this.getNames("Middag", mondayDate, who), this.getNames("Middag", tuesdayDate, who), this.getNames("Middag", wednesdayDate, who), this.getNames("Middag", thursdayDate, who), this.getNames("Middag", fridayDate, who), this.getNames("Middag", saturdayDate, who), this.getNames("Middag", sundayDate, who)},
                    {"Avond 17:00 - 21:00", this.getNames("Avond", mondayDate, who), this.getNames("Avond", tuesdayDate, who), this.getNames("Avond", wednesdayDate, who), this.getNames("Avond", thursdayDate, who), this.getNames("Avond", fridayDate, who), this.getNames("Avond", saturdayDate, who), this.getNames("Avond", sundayDate, who)}
                },
                new String[]{
                    "", "Maandag - " + scheduleManager.getDateString(mondayDate), "Dinsdag - " + scheduleManager.getDateString(tuesdayDate), "Woensdag - " + scheduleManager.getDateString(wednesdayDate), "Donderdag - " + scheduleManager.getDateString(thursdayDate), "Vrijdag - " + scheduleManager.getDateString(fridayDate), "Zaterdag - " + scheduleManager.getDateString(saturdayDate), "Zondag - " + scheduleManager.getDateString(sundayDate)
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int columnIndex) {
                return canEdit[columnIndex];
            }
        };

        schedule_table.setModel(model2);
        this.updateRowHeights(schedule_table);
    }

    /**
     * Creates new form ContactEditorUI
     */
    public Dashboard_GUI(int functionId, int userId, EmployeeManager employeeManager, AvailabilityManager availabilityManager, ScheduleManager scheduleManager) throws ParseException {
        this.employeeManager = employeeManager;
        this.availabilityManager = availabilityManager;
        this.scheduleManager = scheduleManager;
        
        setTitle("Hartige Hap - Planning - Dashboard");
        

        this.functionId = functionId;

        mondayDate = scheduleManager.thisWeekMonday();
        tuesdayDate = scheduleManager.datePlus(mondayDate, 1);
        wednesdayDate = scheduleManager.datePlus(mondayDate, 2);
        thursdayDate = scheduleManager.datePlus(mondayDate, 3);
        fridayDate = scheduleManager.datePlus(mondayDate, 4);
        saturdayDate = scheduleManager.datePlus(mondayDate, 5);
        sundayDate = scheduleManager.datePlus(mondayDate, 6);

        who = userId;

        if (functionId == 3 || functionId == 5) {
            who = 0;
        }

        modelv = new Vector();

        modelv.addElement(new SelectItem(0, "Alle medewerkers"));

        ArrayList<Employee> employeesArrayList = employeeManager.getEmployees();
        employeesArrayList.forEach((Employee employee) -> {
            SelectItem selectItem = new SelectItem(employee.getEmployeeId(), employee.getFullName());
            modelv.addElement(selectItem);
        });

        model1 = new DefaultTableModel(
                new Object[][]{
                    {"Ochtend 09:00 - 13:00", this.getNames("Ochtend", mondayDate, who), this.getNames("Ochtend", tuesdayDate, who), this.getNames("Ochtend", wednesdayDate, who), this.getNames("Ochtend", thursdayDate, who), this.getNames("Ochtend", fridayDate, who), this.getNames("Ochtend", saturdayDate, who), this.getNames("Ochtend", sundayDate, who)},
                    {"Middag 13:00 - 17:00", this.getNames("Middag", mondayDate, who), this.getNames("Middag", tuesdayDate, who), this.getNames("Middag", wednesdayDate, who), this.getNames("Middag", thursdayDate, who), this.getNames("Middag", fridayDate, who), this.getNames("Middag", saturdayDate, who), this.getNames("Middag", sundayDate, who)},
                    {"Avond 17:00 - 21:00", this.getNames("Avond", mondayDate, who), this.getNames("Avond", tuesdayDate, who), this.getNames("Avond", wednesdayDate, who), this.getNames("Avond", thursdayDate, who), this.getNames("Avond", fridayDate, who), this.getNames("Avond", saturdayDate, who), this.getNames("Avond", sundayDate, who)}
                },
                new String[]{
                    "", "Maandag - " + scheduleManager.getDateString(mondayDate), "Dinsdag - " + scheduleManager.getDateString(tuesdayDate), "Woensdag - " + scheduleManager.getDateString(wednesdayDate), "Donderdag - " + scheduleManager.getDateString(thursdayDate), "Vrijdag - " + scheduleManager.getDateString(fridayDate), "Zaterdag - " + scheduleManager.getDateString(saturdayDate), "Zondag - " + scheduleManager.getDateString(sundayDate)
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int columnIndex) {
                return canEdit[columnIndex];
            }
        };

        initComponents();
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() throws ParseException {

        jScrollPane2 = new javax.swing.JScrollPane();
        schedule_table = new javax.swing.JTable();
        schedule_table.setEnabled(false);
        prev_button = new javax.swing.JButton();
        next_button = new javax.swing.JButton();
        today_button = new javax.swing.JButton();
        select_employee_button = new javax.swing.JComboBox<>(modelv);
        jMenuBar1 = new javax.swing.JMenuBar();
        add_schedule_button = new javax.swing.JButton();
        employee_management_button = new javax.swing.JButton();
        availiability_button = new javax.swing.JButton();
        attendance_button = new javax.swing.JButton();
        achievements_button = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        schedule_table.setModel(model1);

        this.updateRowHeights(schedule_table);

        jScrollPane2.setViewportView(schedule_table);
        if (schedule_table.getColumnModel().getColumnCount() > 0) {
            schedule_table.getColumnModel().getColumn(0).setResizable(true);
            schedule_table.getColumnModel().getColumn(1).setResizable(true);
            schedule_table.getColumnModel().getColumn(2).setResizable(true);
            schedule_table.getColumnModel().getColumn(3).setResizable(true);
            schedule_table.getColumnModel().getColumn(4).setResizable(true);
            schedule_table.getColumnModel().getColumn(5).setResizable(true);
            schedule_table.getColumnModel().getColumn(6).setResizable(true);
            schedule_table.getColumnModel().getColumn(7).setResizable(true);

            schedule_table.getColumnModel().getColumn(0).setPreferredWidth(33);
        }

        prev_button.setText("<<");
        prev_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        prev_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    prev_buttonActionPerformed();
                } catch (ParseException ex) {
                    Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        next_button.setText(">>");
        next_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        next_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    next_buttonActionPerformed();
                } catch (ParseException ex) {
                    Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        today_button.setText("Vandaag");
        today_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        today_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    today_buttonActionPerformed();
                } catch (ParseException ex) {
                    Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        select_employee_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    select_employee_buttonActionPerformed();
                } catch (ParseException ex) {
                    Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        if (functionId == 5 || functionId == 3) {
            add_schedule_button.setText("Toevoegen Planning");
            add_schedule_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            add_schedule_button.addActionListener((java.awt.event.ActionEvent evt) -> {
                add_schedule_buttonActionPerformed();
            });
            jMenuBar1.add(add_schedule_button);
        }

        if (functionId == 5 || functionId == 3) {
            employee_management_button.setText("Personeelsbeheer");
            employee_management_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            employee_management_button.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    employee_management_buttonActionPerformed();
                }
            });
            jMenuBar1.add(employee_management_button);
        }

        if (functionId == 5 || functionId == 3) {
            availiability_button.setText("Beschikbaarheid");
            availiability_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            availiability_button.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    availiability_buttonActionPerformed();
                }
            });
            jMenuBar1.add(availiability_button);
        }

        if (functionId == 5 || functionId == 3) {
            attendance_button.setText("Aanwezigheid");
            attendance_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            attendance_button.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    try {
                        attendance_buttonActionPerformed();
                    } catch (HeadlessException ex) {
                        Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(Dashboard_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            jMenuBar1.add(attendance_button);
        }

        if (functionId == 3) {
            achievements_button.setText("Prestaties");
            achievements_button.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            achievements_button.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
            achievements_button.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    achievements_buttonActionPerformed();
                }
            });
            jMenuBar1.add(achievements_button);
        }

        setJMenuBar(jMenuBar1);

        legend = new JLabel("<html><span>&nbsp;&nbsp;Legenda:&nbsp;</span><span style='color: red'>&nbsp;&nbsp;Chefkok&nbsp;&nbsp;&nbsp;</span>"
                + "<span style='color: green'>&nbsp;&nbsp;Bediening&nbsp;&nbsp;</span>"
                + "<span style='color: blue'>&nbsp;&nbsp;Planning&nbsp;&nbsp;&nbsp;</span>"
                + "<span style='color: orange'>&nbsp;&nbsp;Kok&nbsp;&nbsp;</span></html>");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(prev_button)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(today_button)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(next_button)
                                        .addComponent(legend)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGap(700, 700, 700)
                                        .addComponent(select_employee_button, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1350, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(prev_button)
                                .addComponent(today_button)
                                .addComponent(next_button)
                                .addComponent(legend)
                                .addComponent(select_employee_button, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                        .addGap(24, 24, 24))
        );

        pack();
        
        // center frame
        setLocationRelativeTo(null);
        
        if (functionId != 5 && functionId != 3) {
            System.out.println(functionId);
            select_employee_button.setVisible(false);
        }
    }// </editor-fold>     

    private void prev_buttonActionPerformed() throws ParseException {
        mondayDate = scheduleManager.datePlus(mondayDate, -7);
        tuesdayDate = scheduleManager.datePlus(mondayDate, 1);
        wednesdayDate = scheduleManager.datePlus(mondayDate, 2);
        thursdayDate = scheduleManager.datePlus(mondayDate, 3);
        fridayDate = scheduleManager.datePlus(mondayDate, 4);
        saturdayDate = scheduleManager.datePlus(mondayDate, 5);
        sundayDate = scheduleManager.datePlus(mondayDate, 6);

        this.refreshTable();
    }

    private void next_buttonActionPerformed() throws ParseException {
        mondayDate = scheduleManager.datePlus(mondayDate, 7);
        tuesdayDate = scheduleManager.datePlus(mondayDate, 1);
        wednesdayDate = scheduleManager.datePlus(mondayDate, 2);
        thursdayDate = scheduleManager.datePlus(mondayDate, 3);
        fridayDate = scheduleManager.datePlus(mondayDate, 4);
        saturdayDate = scheduleManager.datePlus(mondayDate, 5);
        sundayDate = scheduleManager.datePlus(mondayDate, 6);

        this.refreshTable();
    }

    private void today_buttonActionPerformed() throws ParseException {
        mondayDate = scheduleManager.thisWeekMonday();
        tuesdayDate = scheduleManager.datePlus(mondayDate, 1);
        wednesdayDate = scheduleManager.datePlus(mondayDate, 2);
        thursdayDate = scheduleManager.datePlus(mondayDate, 3);
        fridayDate = scheduleManager.datePlus(mondayDate, 4);
        saturdayDate = scheduleManager.datePlus(mondayDate, 5);
        sundayDate = scheduleManager.datePlus(mondayDate, 6);

        this.refreshTable();
    }
    
    private void add_schedule_buttonActionPerformed() {
        scheduleManager.buildScheduleCreateGui(scheduleManager, this);
    }

    private void employee_management_buttonActionPerformed() {
        try {
            employeeManager.buildEmployeeManagement_GUI();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void availiability_buttonActionPerformed() {
        availabilityManager.buildAvailabilityGui(employeeManager);

    }

    private void attendance_buttonActionPerformed() throws HeadlessException, ParseException {
        availabilityManager.buildPresentGui();
    }

    private void achievements_buttonActionPerformed() {
        availabilityManager.buildPerformanceOverviewGUI(employeeManager);
    }

    private void select_employee_buttonActionPerformed() throws ParseException {
        SelectItem employeeselect = (SelectItem) select_employee_button.getSelectedItem();
        who = employeeselect.getId();
        this.refreshTable();
    }

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify                     
    private javax.swing.JButton achievements_button;
    private javax.swing.JButton add_schedule_button;
    private javax.swing.JButton attendance_button;
    private javax.swing.JButton availiability_button;
    private javax.swing.JButton employee_management_button;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton next_button;
    private javax.swing.JButton prev_button;
    private javax.swing.JTable schedule_table;
    private javax.swing.JComboBox<String> select_employee_button;
    private javax.swing.JButton today_button;
    // End of variables declaration                   
}
