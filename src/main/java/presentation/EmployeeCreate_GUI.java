/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import javax.swing.JPanel;
import businesslogic.*;
import com.toedter.calendar.JDateChooser;
import domain.Function;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Chiel
 */
public class EmployeeCreate_GUI extends JFrame {

    EmployeeCreate_Panel employeeCreatePanel;

    public EmployeeCreate_GUI(EmployeeManager employeeManager, FunctionManager functionManager) throws HeadlessException {
        employeeCreatePanel = new EmployeeCreate_Panel(this, employeeManager, functionManager);

        setSize(500, 700);
        setTitle("Hartige Hap - Planning - Personeelsbeheer - Toevoegen");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setContentPane(employeeCreatePanel);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    class EmployeeCreate_Panel extends JPanel { 
        
        JComboBox functionList;

        EmployeeCreate_GUI window;
        EmployeeManager employeeManager;
        FunctionManager functionManager;

        String firstname, infixname, lastname, email, phonenumber, birthday, joindate, function, username, password, accountnumber, adress, postalcode;
        int hourlywage, monthlyhours;
        boolean active;
        SimpleDateFormat dateFormattrue = new SimpleDateFormat("yyyy-MM-dd");

        public EmployeeCreate_Panel(EmployeeCreate_GUI window, EmployeeManager employeeManager, FunctionManager functionManager) {
            this.setBorder(new EmptyBorder(10, 10, 10, 10));
            this.window = window;
            this.employeeManager = employeeManager;
            this.functionManager = functionManager;

            setLayout(new GridLayout(17, 4, 4, 4));

            JLabel firstnameLabel = new JLabel("Voornaam*");
            JTextField firstnameField = new JTextField();
            add(firstnameLabel);
            add(firstnameField);
            firstnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel infixnameLabel = new JLabel("Tussenvoegsel");
            JTextField infixnameField = new JTextField();
            add(infixnameLabel);
            add(infixnameField);
            infixnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel lastnameLabel = new JLabel("Achternaam*");
            JTextField lastnameField = new JTextField();
            add(lastnameLabel);
            add(lastnameField);
            lastnameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel birthdayLabel = new JLabel("Geboortedatum*");
            JDateChooser birthdayChooser = new JDateChooser();
            birthdayChooser.getDateEditor().setEnabled(false);
            add(birthdayLabel);
            add(birthdayChooser);

            JLabel emailLabel = new JLabel("E-mail");
            JTextField emailField = new JTextField();
            add(emailLabel);
            add(emailField);
            emailField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel phonenumberLabel = new JLabel("Telefoonnummer");
            JTextField phonenumberField = new JTextField();
            add(phonenumberLabel);
            add(phonenumberField);
            phonenumberField.setDocument(new EmployeeManagement_JTextFieldLimit(14));
            
            JLabel accountnumberLabel = new JLabel("Rekeningnummer");
            JTextField accountnumberField = new JTextField();
            add(accountnumberLabel);
            add(accountnumberField);
            accountnumberField.setDocument(new EmployeeManagement_JTextFieldLimit(21));

            JLabel adressLabel = new JLabel("Straat + huisummer");
            JTextField adressField = new JTextField();
            add(adressLabel);
            add(adressField);
            adressField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel postalCodeLabel = new JLabel("Postcode");
            JTextField postalCodeField = new JTextField();
            add(postalCodeLabel);
            add(postalCodeField);
            postalCodeField.setDocument(new EmployeeManagement_JTextFieldLimit(7));

            JLabel joinDateLabel = new JLabel("Datum in dienst treden");
            JDateChooser joinDateChooser = new JDateChooser();
            joinDateChooser.getDateEditor().setEnabled(false);
            add(joinDateLabel);
            add(joinDateChooser);

            JLabel hourlywageLabel = new JLabel("Uurloon");
            SpinnerModel sm1 = new SpinnerNumberModel(0, 0, 200, 1);
            JSpinner hourlywageSpinner = new JSpinner(sm1);
            add(hourlywageLabel);
            add(hourlywageSpinner);

            JLabel monthlyhoursLabel = new JLabel("Contracturen");
            SpinnerModel sm2 = new SpinnerNumberModel(0, 0, 50, 1);
            JSpinner monthlyhoursSpinner = new JSpinner(sm2);
            add(monthlyhoursLabel);
            add(monthlyhoursSpinner);

            JLabel functionLabel = new JLabel("Functie");
            Vector model = new Vector();
                ArrayList<Function> functionsArrayList = functionManager.getFunctions();
                functionsArrayList.forEach((Function function) -> {
                    model.addElement(new SelectItem(function.getFunctionId(), function.getFunctionName()));
                });
            functionList = new JComboBox(model);
            add(functionLabel);
            add(functionList);
            
            JLabel activLabel = new JLabel("Actief");
            JCheckBox activBox = new JCheckBox();
            activBox.setSelected(true);
            add(activLabel);
            add(activBox);

            JLabel usernameLabel = new JLabel("Gebruikersnaam");
            JTextField usernameField = new JTextField();
            add(usernameLabel);
            add(usernameField);
            usernameField.setDocument(new EmployeeManagement_JTextFieldLimit(45));
            

            JLabel passwordLabel = new JLabel("Wachtwoord");
            JPasswordField passwordField = new JPasswordField();
            add(passwordLabel);
            add(passwordField);
            passwordField.setDocument(new EmployeeManagement_JTextFieldLimit(45));

            JLabel saveLabel = new JLabel("");
            JButton saveButton = new JButton("Opslaan");
            add(saveLabel);
            add(saveButton);

            saveButton.addActionListener((ActionEvent e) -> {
                username = usernameField.getText();
                password = passwordField.getText();
                firstname = firstnameField.getText();
                infixname = infixnameField.getText();
                lastname = lastnameField.getText();
                accountnumber = accountnumberField.getText();
                email = emailField.getText();
                phonenumber = phonenumberField.getText();
                adress = adressField.getText();
                postalcode = postalCodeField.getText();

                Date birthdaycheck = birthdayChooser.getDate();

                if (birthdaycheck == null) {
                    birthday = "";
                } else {
                    birthday = dateFormattrue.format(birthdayChooser.getDate());
                }
                
                Date jooindatecheck = joinDateChooser.getDate();

                if (jooindatecheck == null) {
                    joindate = "";
                } else {
                    joindate = dateFormattrue.format(joinDateChooser.getDate());
                }

                hourlywage = employeeManager.validateInput((int) hourlywageSpinner.getValue());
                monthlyhours = (int) monthlyhoursSpinner.getValue();

                function = ""+this.getFunctionId();

                active = activBox.isSelected();
                

                String result = employeeManager.checkInsert(username, password, firstname, infixname, lastname, birthday, accountnumber, hourlywage, monthlyhours, adress, postalcode, email, phonenumber, joindate, active, function);
                saveLabel.setForeground(Color.red);

                if (employeeManager.checkInsert_Boolean(firstname, lastname, birthday)) {
                    employeeManager.refreshFrame();
                    window.dispose();
                } else {
                    saveLabel.setText(result);
                }

            });
        }
        
        private int getFunctionId() {
            // function
            SelectItem functionItem = (SelectItem) functionList.getSelectedItem();
            int functionId = functionItem.getId();
            
            return functionId;
        }

    }
}
