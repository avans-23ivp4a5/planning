/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.*;
import com.toedter.calendar.JDateChooser;
import domain.Employee;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.*;
import java.text.*;
import java.time.LocalDate;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.*;
import javax.swing.*;

/**
 *
 * @author Robin
 */
public class ScheduleCreate_GUI extends JFrame {
    ScheduleCreate_Panel scheduleCreatePanel;
    
    String selectedDate;
    ArrayList<String> shifts = new ArrayList<>();
    HashMap<String, HashMap> checkBoxList = new HashMap<>();
    
    
    public ScheduleCreate_GUI(ScheduleManager scheduleManager, Dashboard_GUI dashboardGUI) {
        // add shifts
        shifts.add("Ochtend");
        shifts.add("Middag");
        shifts.add("Avond");
        
        shifts.forEach((String shift) -> {
            checkBoxList.put(shift, new HashMap<Integer, JCheckBox>());
        });
        
        // create panel
        scheduleCreatePanel = new ScheduleCreate_Panel(this, scheduleManager, dashboardGUI);
        
        // set all frame properties
        setSize(700, 1000);
        getRootPane().setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        setTitle("De Hartige Hap - Planning - Planning Invoeren");
        setContentPane(scheduleCreatePanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    class ScheduleCreate_Panel extends JPanel {
        
        ScheduleCreate_GUI window;
        Dashboard_GUI dashboardGUI;
        
        ScheduleManager scheduleManager;

        // fields
        JLabel morningLabel, afternoonLabel, eveningLabel, chefMorningLabel, chefAfternoonLabel, chefEveningLabel, cookMorningLabel, cookAfternoonLabel, cookEveningLabel, serverMorningLabel, serverAfternoonLabel, serverEveningLabel, errorLabel;
        JDateChooser dateInput;
        JButton saveButton, prevButton, nextButton;
        JPanel morningChefPanel;
        JCheckBox checkBox;

        // helpers
        SimpleDateFormat dateFormattrue = new SimpleDateFormat("yyyy-MM-dd");

        private ScheduleCreate_Panel(ScheduleCreate_GUI window, ScheduleManager scheduleManager, Dashboard_GUI dashboardGUI) {
            // keep window
            this.window = window;
            
            // dashboard gui
            this.dashboardGUI = dashboardGUI;
            
            // keep managers
            this.scheduleManager = scheduleManager;

            // set layout
            setLayout(new GridLayout(13, 3, 2, 2));
            
            
            if (selectedDate == null) {
                selectedDate = dateFormattrue.format(new Date());
            }
            
//            DefaultTableModel model1 = new DefaultTableModel();
//            
//            Object[] columnsName = new Object[1];
//
//            columnsName[0] = "Naam";
//
//            model1.setColumnIdentifiers(columnsName);
//            Object[] rowData = new Object[3];
//            for (Entry<String, ArrayList> entry : scheduleManager.showAvailability(date).entrySet()) {
//                
//                rowData[0] = employee.getFirstName();
//                rowData[1] = employee.getInfixName();
//                rowData[2] = employee.getLastName();
//                model1.addRow(rowData);
//            }
//            morningTable.setModel(model1);
//            morningTable.setEnabled(true);
 
            // add fields
            initFields();
        }
        
        private void initFields() {
            removeAll();

            //previous button
            prevButton = new JButton("<<");
            add(prevButton);
            
            prevButton.addActionListener((ActionEvent e) -> {
                handlePrevButton();
            });
            
            // datum
            dateInput = new JDateChooser();
            
            // set date if possible
            if (selectedDate != null) {
                try {
                    dateInput.setDate(dateFormattrue.parse(selectedDate));
                } catch (ParseException ex) {
                    Logger.getLogger(ScheduleCreate_GUI.class.getName()).log(Level.OFF, null, ex);
                }
            }
                    
            dateInput.getDateEditor().addPropertyChangeListener((PropertyChangeEvent e) -> {
                Date date = dateInput.getDate();
                Date currentlySelected = null;
                
                try {
                    currentlySelected = dateFormattrue.parse(selectedDate);
                } catch (ParseException ex) {
                    Logger.getLogger(ScheduleCreate_GUI.class.getName()).log(Level.OFF, null, ex);
                }
                
                if (date == null || currentlySelected == null || date.equals(currentlySelected)) {
                    return;
                }
                
                selectedDate = dateFormattrue.format(date);
                scheduleManager.reloadFrame(selectedDate);

                initFields();
            });
            dateInput.getDateEditor().setEnabled(false);
            add(dateInput);
            
            //next button
            nextButton = new JButton(">>");
            add(nextButton);
            
            nextButton.addActionListener((ActionEvent e) -> {
                handleNextButton();
            });
            
            //morning label - 1
            morningLabel = new JLabel("Ochtenddienst");
            morningLabel.setFont(new Font("", Font.BOLD, 16));
            add(morningLabel);
            
            //afternoon label - 2
            afternoonLabel = new JLabel("Middagdienst");
            afternoonLabel.setFont(new Font("", Font.BOLD, 16));
            add(afternoonLabel);
            
            //evening label - 3
            eveningLabel = new JLabel("Avonddienst");
            eveningLabel.setFont(new Font("", Font.BOLD, 16));
            add(eveningLabel);
     
            //chef labels - 4-5-6
            chefMorningLabel = new JLabel ("<html><b>Chefkok</b></html>");
            chefAfternoonLabel = new JLabel ("<html><b>Chefkok</b></html>");
            chefEveningLabel = new JLabel ("<html><b>Chefkok</b></html>");
            add(chefMorningLabel);
            add(chefAfternoonLabel);
            add(chefEveningLabel);
            
            //morningChefTable - 7
            displayEmployees(1);
            
            //cook labels - 10-11-12
            cookMorningLabel = new JLabel("<html><b>Kok</b></html>");
            cookAfternoonLabel = new JLabel("<html><b>Kok</b></html>");
            cookEveningLabel = new JLabel("<html><b>Kok</b></html>");
            add(cookMorningLabel);
            add(cookAfternoonLabel);
            add(cookEveningLabel);
            displayEmployees(4);

            //server labels - 16-17-18
            serverMorningLabel = new JLabel("<html><b>Bediening</b></html>");
            serverAfternoonLabel = new JLabel("<html><b>Bediening</b></html>");
            serverEveningLabel = new JLabel ("<html><b>Bediening</b></html>");
            add(serverMorningLabel);
            add(serverAfternoonLabel);
            add(serverEveningLabel);
            displayEmployees(2);

            //server labels - 16-17-18
            JLabel plannerMorningLabel = new JLabel("<html><b>Planning</b></html>");
            JLabel plannerAfternoonLabel = new JLabel("<html><b>Planning</b></html>");
            JLabel plannerEveningLabel = new JLabel ("<html><b>Planning</b></html>");
            add(plannerMorningLabel);
            add(plannerAfternoonLabel);
            add(plannerEveningLabel);
            displayEmployees(3);

            //server labels - 16-17-18
            JLabel plannerHulpMorningLabel = new JLabel("<html><b>Planning - hulp</b></html>");
            JLabel plannerHulpAfternoonLabel = new JLabel("<html><b>Planning - hulp</b></html>");
            JLabel plannerHulpEveningLabel = new JLabel ("<html><b>Planning - hulp</b></html>");
            add(plannerHulpMorningLabel);
            add(plannerHulpAfternoonLabel);
            add(plannerHulpEveningLabel);
            displayEmployees(5);
            
            // save button
            saveButton = new JButton("Opslaan");
            JButton saveButtonClose = new JButton("Opslaan & Sluiten");
            errorLabel = new JLabel("");
            
            add(saveButton);
            add(errorLabel);
            add(saveButtonClose);

            saveButton.addActionListener((ActionEvent e) -> {
                handleSaveButton(false);
            });
            saveButtonClose.addActionListener((ActionEvent e) -> {
                handleSaveButton(true);
            });
            
            revalidate();
            repaint();
        }

        private void handleSaveButton(boolean close) {
            checkBoxList.entrySet().stream().forEach((Entry<String, HashMap> entry) -> {
                HashMap<Integer, JCheckBox> checkBoxes = entry.getValue();
                checkBoxes.entrySet().stream().forEach((Entry<Integer, JCheckBox> checkBoxEntry) -> {
                    scheduleManager.updateAvailabilityScheduled(selectedDate, entry.getKey(), checkBoxEntry.getKey(), checkBoxEntry.getValue().isSelected());
                });
            });
            
            try {
                dashboardGUI.refreshTable();
            } catch (ParseException ex) {
                Logger.getLogger(ScheduleCreate_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (close) {        
                window.dispose();
            } else {
                errorLabel.setForeground(Color.BLUE);
                errorLabel.setText("Opgeslagen.");
            }
        } 
        
        private void handlePrevButton() {
            try {
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateFormattrue.parse(selectedDate));
                cal.add(Calendar.DATE, -1);  // number of days to add
                dateInput.setDate(cal.getTime());  // dt is now the new date
            } catch (ParseException ex) {
                Logger.getLogger(ScheduleCreate_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        
        private void handleNextButton() {
            try {
                Calendar cal = Calendar.getInstance();
                cal.setTime(dateFormattrue.parse(selectedDate));
                cal.add(Calendar.DATE, 1);  // number of days to add
                dateInput.setDate(cal.getTime());  // dt is now the new date
            } catch (ParseException ex) {
                Logger.getLogger(ScheduleCreate_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 

        private void displayEmployees(int functionId) {
            shifts.forEach((String shift) -> {
                ArrayList<Employee> employees = scheduleManager.getAvailabilities(selectedDate, functionId, shift);
                JPanel panel = new JPanel(new GridLayout(employees.size(), 1));
                HashMap<Integer, JCheckBox> checkBoxes = checkBoxList.get(shift);
                for(Employee employee : employees) {
                    JCheckBox checkbox = new JCheckBox(employee.getFullName());
                    checkbox.setSelected(employee.isScheduled());
                    checkBoxes.put(employee.getEmployeeId(), checkbox);
                    panel.add(checkbox);
                }
                checkBoxList.put(shift, checkBoxes);
                add(panel);
            });
        }
    }
}
