/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import javax.swing.*;
import businesslogic.*;
import domain.Employee;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jari
 */
public class PerformanceOverview_GUI extends JFrame {

    PerformanceOverview_Panel performanceOverview_Panel;

    public PerformanceOverview_GUI(EmployeeManager employeeManager, AvailabilityManager availabilityManager) {
        setSize(475, 250);
        setTitle("Hartige Hap - Planning - PrestatieOverzicht");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        performanceOverview_Panel = new PerformanceOverview_GUI.PerformanceOverview_Panel(this, employeeManager,availabilityManager);
        setContentPane(performanceOverview_Panel);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    class PerformanceOverview_Panel extends JPanel {

        EmployeeManager employeeManager;
        AvailabilityManager availabilityManager;
        PerformanceOverview_GUI window;
        private JLabel titel;
        private JTable performance_table;
        HashMap<Integer,String> workedHoursMonth;

        public PerformanceOverview_Panel(PerformanceOverview_GUI window, EmployeeManager employeeManager, AvailabilityManager availabilityManager) {
            this.employeeManager = employeeManager;
            this.window = window;
            this.availabilityManager = availabilityManager;
            titel = new JLabel("Prestatie Overzicht");
            performance_table = new JTable();
            
            workedHoursMonth = availabilityManager.getWorkedHours();
            
            DefaultTableModel model = new DefaultTableModel();

            Object[] columnsName = new Object[6];

            columnsName[0] = "ID";
            columnsName[1] = "Voornaam";
            columnsName[2] = "Tussenvoegsel";
            columnsName[3] = "Achternaam";
            columnsName[4] = "Contracturen";
            columnsName[5] = "Tijd gewerkt";

            model.setColumnIdentifiers(columnsName);
            Object[] rowData = new Object[6];
            for (Entry<Integer, String> entry : workedHoursMonth.entrySet()) {
                Employee employee = employeeManager.getEmployeeFromDB(entry.getKey());
                rowData[0] = employee.getEmployeeId();
                rowData[1] = employee.getFirstName();
                rowData[2] = employee.getInfixName();
                rowData[3] = employee.getLastName();
                rowData[4] = employee.getMonthlyHours();
                rowData[5] = entry.getValue();
                model.addRow(rowData);
            }

            performance_table.setModel(model);
            performance_table.setEnabled(true);

            JScrollPane pane = new JScrollPane(performance_table);
            pane.setPreferredSize(new Dimension(483, 350));

            performance_table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            if (performance_table.getColumnModel().getColumnCount() > 0) {
                performance_table.getColumnModel().getColumn(0).setResizable(false);
                performance_table.getColumnModel().getColumn(0).setPreferredWidth(20);
                performance_table.getColumnModel().getColumn(1).setResizable(false);
                performance_table.getColumnModel().getColumn(1).setPreferredWidth(80);
                performance_table.getColumnModel().getColumn(2).setResizable(false);
                performance_table.getColumnModel().getColumn(2).setPreferredWidth(100);
                performance_table.getColumnModel().getColumn(3).setResizable(false);
                performance_table.getColumnModel().getColumn(3).setPreferredWidth(80);
                performance_table.getColumnModel().getColumn(4).setResizable(false);
                performance_table.getColumnModel().getColumn(4).setPreferredWidth(80);
                performance_table.getColumnModel().getColumn(5).setResizable(false);
                performance_table.getColumnModel().getColumn(5).setPreferredWidth(85);
                performance_table.setEnabled(false);
            }

            pane.setBorder(new EmptyBorder(10, 10, 0, 0));
            titel.setBorder(new EmptyBorder(5, 10, 0, 0));
            setLayout(new BorderLayout());
            add(titel, BorderLayout.NORTH);
            add(pane, BorderLayout.CENTER);

        }

    }
}
