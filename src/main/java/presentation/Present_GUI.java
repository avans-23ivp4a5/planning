/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import javax.swing.JPanel;
import businesslogic.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Chiel
 */
public class Present_GUI extends JFrame {

    Present_Panel presentCreatePanel;

    public Present_GUI(AvailabilityManager availabilityManager) throws HeadlessException, ParseException {
        presentCreatePanel = new Present_Panel(this, availabilityManager);

        setSize(300, 600);
        setTitle("Hartige Hap - Planning - Personeelsbeheer - Aanwezigheid");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setContentPane(presentCreatePanel);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    class Present_Panel extends JPanel {

        Present_GUI window;
        AvailabilityManager availabilityManager;

        public Present_Panel(Present_GUI window, AvailabilityManager availabilityManager) throws ParseException {
            this.setBorder(new EmptyBorder(10, 10, 10, 10));
            this.window = window;
            this.availabilityManager = availabilityManager;

            setLayout(new GridLayout(17, 4, 4, 4));

            ArrayList<String> presentNames = new ArrayList<>();
            presentNames = availabilityManager.getPresent();

            JLabel title = new JLabel("Aanwezigheidsoverzicht");
            add(title);

            JLabel presentlabel = new JLabel("Aanwezig:");
            presentlabel.setFont(new Font("", Font.BOLD, 20));
            add(presentlabel);

            if (presentNames.isEmpty()) {
                JLabel nobodypresentpersonlabel = new JLabel("Er is niemand aanwezig.");
                nobodypresentpersonlabel.setForeground(Color.blue);
                add(nobodypresentpersonlabel);
            }

            for (String present : presentNames) {
                JLabel presentpersonlabel = new JLabel(present);
                add(presentpersonlabel);
            }

            JLabel filler2 = new JLabel();
            add(filler2);

            ArrayList<String> notPresentNames = new ArrayList<>();
            notPresentNames = availabilityManager.getNotPresent();

            JLabel notpresentlabel = new JLabel("Ongeoorloofd afwezig:");
            notpresentlabel.setFont(new Font("", Font.BOLD, 20));
            add(notpresentlabel);

            if (notPresentNames.isEmpty()) {
                JLabel notnotpresentpersonlabel = new JLabel("Er is niemand ongeoorloofd afwezig.");
                notnotpresentpersonlabel.setForeground(Color.blue);
                add(notnotpresentpersonlabel);
            }

            for (String notpresent : notPresentNames) {
                JLabel notpresentpersonlabel = new JLabel(notpresent);
                notpresentpersonlabel.setForeground(Color.red);
                add(notpresentpersonlabel);
            }

        }
    }
}
