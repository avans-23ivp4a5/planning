/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.*;
import com.toedter.calendar.JDateChooser;
import domain.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Robin
 */
public class AvailabilityCreate_GUI extends JFrame {
    AvailabilityCreate_Panel availabilityCreatePanel;
    
    public AvailabilityCreate_GUI(AvailabilityManager availabilityManager, EmployeeManager employeeManager) {
        // create panel
        availabilityCreatePanel = new AvailabilityCreate_Panel(this, availabilityManager, employeeManager);
        
        // set all frame properties
        setSize(500, 450);
        setTitle("Hartige Hap - Planning - Beschikbaarheid Invoeren");
        setContentPane(availabilityCreatePanel);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    class AvailabilityCreate_Panel extends JPanel {
        
        AvailabilityCreate_GUI window;
        
        AvailabilityManager availabilityManager;
        EmployeeManager employeeManager;

        // data
        String date;
        int employeeId;
        ArrayList<String> shifts = new ArrayList<>();
        ArrayList<Availability> availabilities = new ArrayList<>();

        // fields
        JLabel employeeLabel, repeatLabel, dateLabel, shiftLabel, errorLabel;
        JComboBox employeeInput;
        JDateChooser dateInput;
        JSpinner repeatInput;
        JCheckBox morningCheckbox, afternoonCheckbox, eveningCheckbox; 
        JButton saveButton;

        // helpers
        SimpleDateFormat dateFormattrue = new SimpleDateFormat("yyyy-MM-dd");

        private AvailabilityCreate_Panel(AvailabilityCreate_GUI window, AvailabilityManager availabilityManager, EmployeeManager employeeManager) {
            // keep window
            this.window = window;
            
            // keep managers
            this.availabilityManager = availabilityManager;
            this.employeeManager = employeeManager;

            // set layout
            setLayout(new GridLayout(5, 2, 4, 4));
            setBorder(new EmptyBorder(10, 10, 10, 10));
            
            // add fields
            initFields();
        }

        private void initFields() {
            // employee field
            addEmployeeInput();
            
            // datum
            dateLabel = new JLabel("Datum*");
            dateInput = new JDateChooser();
            dateInput.getDateEditor().setEnabled(false);
            add(dateLabel);
            add(dateInput);
            
            // shift
            addShiftInputs();

            // repeat
            repeatLabel = new JLabel("Wekelijkse herhaling");
            repeatInput = new JSpinner(new SpinnerNumberModel(0, 0, 10, 1));
            repeatInput.setEditor(new JSpinner.DefaultEditor(repeatInput));
            add(repeatLabel);
            add(repeatInput);
            
            // save button
            errorLabel = new JLabel("");
            errorLabel.setForeground(Color.red);
            saveButton = new JButton("Opslaan");
            add(errorLabel);
            add(saveButton);

            saveButton.addActionListener((ActionEvent e) -> {
                handleSaveButton();
            });
        }

        private void addEmployeeInput() {
            employeeLabel = new JLabel("Medewerker*");
            Vector model = new Vector();
            
            ArrayList<Employee> employeesArrayList = employeeManager.getEmployees();
            employeesArrayList.forEach((Employee employee) -> {
                model.addElement(new SelectItem(employee.getEmployeeId(), employee.getFullName()));
            });
            
            employeeInput = new JComboBox(model);
            add(employeeLabel);
            add(employeeInput);
        }
        
        private void addShiftInputs() {
            shiftLabel = new JLabel("Dagdeel");
            JPanel shiftPanel= new JPanel();
            shiftPanel.setLayout(new GridLayout(3,1));
            
            // create checkboxes
            morningCheckbox = new JCheckBox("Ochtend");
            afternoonCheckbox = new JCheckBox("Middag");
            eveningCheckbox = new JCheckBox("Avond");
            
            // add to panel
            shiftPanel.add(morningCheckbox);
            shiftPanel.add(afternoonCheckbox);
            shiftPanel.add(eveningCheckbox);
            add(shiftLabel);
            add(shiftPanel);
        }

        private void handleSaveButton() {
            employeeId = getEmployeeId();

            // selected shifts
            if (morningCheckbox.isSelected()) {
                shifts.add("Ochtend");
            }
            if (afternoonCheckbox.isSelected()) {
                shifts.add("Middag");
            }
            if (eveningCheckbox.isSelected()) {
                shifts.add("Avond");
            }
            
            // validate input
            Date date = dateInput.getDate();
            String result = availabilityManager.validateInput(date);
            if (!result.equals("")) {
                errorLabel.setText(result);
                return;
            }
            
            // repeat
            int repeat = (int) repeatInput.getValue();

            // insert availabilities into db
            availabilityManager.insertNew(repeat, employeeId, date, shifts);
            
            // close frame
            window.dispose();
        }
        
        private int getEmployeeId()
        {
            // employee
            SelectItem employeeItem = (SelectItem) employeeInput.getSelectedItem();
            employeeId = employeeItem.getId();
            
            return employeeId;
        }
        
    }
}
