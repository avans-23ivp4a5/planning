/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import businesslogic.ScheduleManager;
import businesslogic.SignInManager;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Chiel
 */
public class SignIn_GUI extends JFrame {

    public SignIn_GUI(SignInManager signInManager) {
        // create panel
        SignInPanel signinPanel = new SignInPanel(this, signInManager);

        // set all frame properties
        setSize(300, 250);
        setTitle("Hartige Hap - Planning - Inloggen");
        setContentPane(signinPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true); 
    }

    class SignInPanel extends JPanel {

        public SignInPanel(JFrame frame, SignInManager signInManager) {

            this.setBorder(new EmptyBorder(10, 10, 10, 10));
            setLayout(new GridLayout(6, 1, 4, 4));

            JLabel usernamelabel = new JLabel("Gebruikersnaam");
            JTextField usernamefield = new JTextField();
            JLabel passwordlabel = new JLabel("Wachtwoord");
            JPasswordField passwordfield = new JPasswordField();
            JButton loginbutton = new JButton("Inloggen");
            JLabel message = new JLabel("");

            add(usernamelabel);
            add(usernamefield);
            add(passwordlabel);
            add(passwordfield);
            add(loginbutton);
            add(message);

            loginbutton.addActionListener((ActionEvent e) -> {
                try {
                    signInManager.check(usernamefield.getText(), passwordfield.getText(), message, frame);
                } catch (ParseException ex) {
                    Logger.getLogger(SignIn_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

            usernamefield.addActionListener((ActionEvent e) -> {
                try {
                    signInManager.check(usernamefield.getText(), passwordfield.getText(), message, frame);
                } catch (ParseException ex) {
                    Logger.getLogger(SignIn_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

            passwordfield.addActionListener((ActionEvent e) -> {
                try {
                    signInManager.check(usernamefield.getText(), passwordfield.getText(), message, frame);
                } catch (ParseException ex) {
                    Logger.getLogger(SignIn_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            });
        }
    }

}
