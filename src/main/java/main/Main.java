/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import businesslogic.*;

/**
 *
 * @author Jari
 */
class Main {

    private Main() {
    }
    

    public static void main(String[] args) {
        //functie
        FunctionManager functionManager = new FunctionManager();
        
        // planning
        ScheduleManager scheduleManager = new ScheduleManager();
        
        // empolyees
        EmployeeManager employeeManager = new EmployeeManager(functionManager);

        // availability
        AvailabilityManager availabilityManager = new AvailabilityManager(employeeManager);
        
        // sign in
        SignInManager signInManager = new SignInManager(scheduleManager, employeeManager, availabilityManager);
        signInManager.startGui();
    }
}