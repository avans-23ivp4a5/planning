/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import domain.Employee;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import presentation.Dashboard_GUI;
import presentation.ScheduleCreate_GUI;
import datastorage.AvailabilityDAO;
import java.util.*;

/**
 *
 * @author Jari
 */
public class ScheduleManager {

    AvailabilityDAO availabilityDAO = new AvailabilityDAO();

    public void startDashboardGui(int functionId,int userId, EmployeeManager employeeManager, AvailabilityManager availabilityManager) throws ParseException {
        //System.out.println("Nu moet het dashboard gestart worden");
        Dashboard_GUI dashboardGui = new Dashboard_GUI(functionId,userId, employeeManager, availabilityManager, this);

    }

    public Date todayDate() {
        Date date = Calendar.getInstance().getTime();
        return date;
    }

    public String todayDay() {
        Date date = this.todayDate();
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
        return dayOfWeek;
    }

    public Date thisWeekMonday() {
        String todayDay = this.todayDay();
        Date date = this.todayDate();

        if (todayDay.equals("Tuesday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -1);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        if (todayDay.equals("Wednesday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -2);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        if (todayDay.equals("Thursday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -3);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        if (todayDay.equals("Friday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -4);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        if (todayDay.equals("Saturday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -5);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        if (todayDay.equals("Sunday")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, -6);  // number of days to add
            date = cal.getTime();  // dt is now the new date
        }

        return date;
    }

    public ArrayList<Employee> scheduled(String shift, Date date, int who) throws ParseException {
        Format sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdf.format(date);
        ArrayList<Employee> scheduledEmployees = new ArrayList<>();
        scheduledEmployees = availabilityDAO.getScheduled(shift, dateString, who);
        
        return scheduledEmployees;
    }

    public void startDashboardGui(int functionId,int userId, EmployeeManager employeeManager, AvailabilityManager availabilityManager, ScheduleManager scheduleManager) throws ParseException {
        //System.out.println("Nu moet het dashboard gestart worden");
        Dashboard_GUI dashboardGui = new Dashboard_GUI(functionId, userId, employeeManager, availabilityManager, scheduleManager);
    }

    public void buildScheduleCreateGui(ScheduleManager scheduleManager, Dashboard_GUI dashboardGUI) {
        ScheduleCreate_GUI scheduleCreateGui = new ScheduleCreate_GUI(scheduleManager, dashboardGUI);
    }
    
    public ArrayList<Employee> getAvailabilities(String date, int functionId, String shift){
        return availabilityDAO.search(date, functionId, shift);
    }

    public Date datePlus(Date date, int aantal) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, aantal);  // number of days to add
        date = cal.getTime();  // dt is now the new date
        return date;
    }

    public String getDateString(Date date) {
        String dateString;
        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
        dateString = formatter.format(date);
        return dateString;
    }
    
    public void reloadFrame(String date) {
        
    }

    public void updateAvailabilityScheduled(String selectedDate, String shift, Integer employeeId, boolean scheduled) {
        availabilityDAO.updateScheduled(selectedDate, shift, employeeId, scheduled);
    }

}
