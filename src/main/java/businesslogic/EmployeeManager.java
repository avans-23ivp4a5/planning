/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import domain.Employee;
import datastorage.EmployeeDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import presentation.*;

/**
 *
 * @author Jari
 */
public class EmployeeManager {

    private ArrayList<Employee> employees;
    EmployeeManagement_GUI guiEmployeeManagement;
    FunctionManager functionManager;

    public EmployeeManager(FunctionManager functionManager) {
        loadEmployees();
        this.functionManager = functionManager;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    private void loadEmployees() {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employees = employeeDAO.findEmployees();
    }

    public ArrayList<Employee> getEmployees() {
        if (employees.isEmpty()) {
            loadEmployees();
        }

        return employees;
    }

    public String checkInsert(String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, Boolean active, String functionId) {
        if (firstName.equals("") || lastName.equals("") || dateOfBirth.equals("")) {
            return "*Niet alle verplichte velden zijn ingevuld!";
        } else {
            this.insertNew(username, password, firstName, infixName, lastName, dateOfBirth, accountNumber, hourlyWage, monthlyHours, address, postalCode, email, phoneNo, joinDate, active, functionId);
            //framenewperson.dispose();
            return "Geslaagd!";
        }
    }

    public Boolean checkInsert_Boolean(String firstName, String lastName, String dateOfBirth) {
       return !(firstName.equals("") || lastName.equals("") || dateOfBirth.equals(""));
    }

    public String checkInsertUpdate(int employeeId, String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, Boolean active, String functionId) {
        if (firstName.equals("") || lastName.equals("") || dateOfBirth.equals("")) {
            return "*Niet alle verplichte velden zijn ingevuld!";

        } else {
            this.insertUpdate(employeeId, username, password, firstName, infixName, lastName, dateOfBirth, accountNumber, hourlyWage, monthlyHours, address, postalCode, email, phoneNo, joinDate, active, functionId);
            //framenewperson.dispose();
            return "Geslaagd!";
        }
    }

    public Boolean checkInsertUpdate_boolean(int employeeId, String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, Boolean active, String functionId) {
        return firstName.equals("") || lastName.equals("") || dateOfBirth.equals("");
    }

    public void insertNew(String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, Boolean active, String functionId) {
        int activeint;
        if (active) {
            activeint = 1;
        } else {
            activeint = 0;
        }
        EmployeeDAO dao = new EmployeeDAO();
        dao.insertEmployee(username, password, firstName, infixName, lastName, dateOfBirth, accountNumber, hourlyWage, monthlyHours, address, postalCode, email, phoneNo, joinDate, activeint, functionId);
    }

    public void insertUpdate(int employeeId, String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, Boolean active, String functionId) {
        int activeint;
        if (active != false) {
            activeint = 1;
        } else {
            activeint = 0;
        }
        EmployeeDAO dao = new EmployeeDAO();
        dao.updateEmployee(employeeId, username, password, firstName, infixName, lastName, dateOfBirth, accountNumber, hourlyWage, monthlyHours, address, postalCode, email, phoneNo, joinDate, activeint, functionId);
    }

    public Employee getEmployeeFromDB(int emplyee_id) {
        EmployeeDAO dao = new EmployeeDAO();
        Employee changer = dao.find(emplyee_id);
        return changer;
    }

    public Date convertdate(String dateIN) throws ParseException {
        SimpleDateFormat dateFormattrue = new SimpleDateFormat("yyyy-MM-dd");
        Date dateGOOD = dateFormattrue.parse(dateIN);
        return dateGOOD;
    }

    public void buildEmployeeEditGUI(int id) throws ParseException {
        EmployeeEdit_GUI employeeEdit = new EmployeeEdit_GUI(id, this, functionManager);
    }

    public void buildEmployeeCreateGUI() throws ParseException {
        EmployeeCreate_GUI startgui = new EmployeeCreate_GUI(this, functionManager);
    }

    public void buildEmployeeManagement_GUI() {
        guiEmployeeManagement = new EmployeeManagement_GUI(this);
    }

    public void refreshFrame() {
        loadEmployees();
        guiEmployeeManagement.dispose();
        guiEmployeeManagement = new EmployeeManagement_GUI(this);
    }
        
    public int validateInput(int hourlyWage) {
        if (hourlyWage > 200) {
            return 0;
        }

        if (hourlyWage <= 0) {
            return 0;
        }

        return hourlyWage;
    }
}
