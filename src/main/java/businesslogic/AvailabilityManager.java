/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import datastorage.*;
import domain.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;
import presentation.*;
import presentation.AvailabilityCreate_GUI;
import presentation.Present_GUI;

/**
 *
 * @author Jari
 */
public class AvailabilityManager {

    private AvailabilityDAO availabilityDAO = new AvailabilityDAO();
    private EmployeeManager employeeManager;
    private HashMap<Integer,String> workedHoursMonth;


    public AvailabilityManager(EmployeeManager employeeManager) {
        this.employeeManager = employeeManager;
    }

    public String validateInput(Date date) {
        if (date == null) {
            return "Geen datum gekozen!";
        }

        return "";
    }

    public void insertNew(int repeat, int employeeId, Date date, ArrayList<String> shifts) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        for (int i = 0; i <= repeat; i++) {
            String formatedDate = sdf.format(c.getTime());
            availabilityDAO.removeShiftsOnDay(employeeId, formatedDate);

            // insert all the availabilities that dont excist
            shifts.forEach((String shift) -> {
                Availability availability = new Availability(formatedDate, shift, employeeId, null, null, false);

                availabilityDAO.insert(availability);
            });

            c.add(Calendar.DATE, 7);  // number of days to add
        }
    }

    public boolean canChange(int employeeId, String date) {
        int amount = availabilityDAO.countShifts(employeeId, date);

        return amount == 0;
    }

    public void buildAvailabilityGui(EmployeeManager employeeManager) {
        AvailabilityCreate_GUI startgui = new AvailabilityCreate_GUI(this, employeeManager);
    }
    
    public void buildPerformanceOverviewGUI(EmployeeManager employeeManager){
        PerformanceOverview_GUI performanceOverviewGUI = new PerformanceOverview_GUI(employeeManager,this);
    }
    
    public HashMap<Integer,String> getWorkedHours() {
       workedHoursMonth = availabilityDAO.getWorkedHoursMonth();
        
        return workedHoursMonth;
    }
    
    public void buildPresentGui() throws HeadlessException, ParseException {
        Present_GUI startgui = new Present_GUI(this);
    }

    public ArrayList getPresent() throws ParseException {

        ArrayList<Employee> presentEmployees = new ArrayList<>();
        presentEmployees = availabilityDAO.getPresents();
        ArrayList<String> presentNames = new ArrayList<>();

        for (Employee employee : presentEmployees) {

            presentNames.add(employee.getFullName());

        }
        return presentNames;
    }
    
    public ArrayList getNotPresent() throws ParseException{
        
        ArrayList<Employee> notPresentEmployees = new ArrayList<>();
        notPresentEmployees = availabilityDAO.getNotPresents();
        ArrayList<String> notPresentNames = new ArrayList<>();

        for (Employee employee : notPresentEmployees) {

            notPresentNames.add(employee.getFullName());

        }
        return notPresentNames;
        
    }
}
