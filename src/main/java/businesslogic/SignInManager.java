/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import datastorage.EmployeeDAO;
import domain.Employee;
import java.awt.Color;
import java.text.ParseException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import presentation.SignIn_GUI;

/**
 *
 * @author Chiel
 */
public class SignInManager {

    ScheduleManager scheduleManager;
    EmployeeManager employeeManager;
    AvailabilityManager availabilityManager;

    public SignInManager(ScheduleManager scheduleManager, EmployeeManager employeeManager, AvailabilityManager availabilityManager) {
        this.scheduleManager = scheduleManager;
        this.employeeManager = employeeManager;
        this.availabilityManager = availabilityManager;
    }

    public void check(String username, String password, JLabel label, JFrame frame) throws ParseException {
        int functionId;
        String returnvalue;
        EmployeeDAO EmployeeDAO = new EmployeeDAO();
        Employee returnuser = EmployeeDAO.checkUsername(username);
        if (returnuser == null) {
            returnvalue = "no_user";
        } else {
            returnvalue = returnuser.getPassword();
        }
        label.setForeground(Color.red);
        label.setText("Een ongedefineerde fout is opgetreden!");
        if (returnvalue.equals("no_user")) {
            label.setText("Deze gebruikersnaam bestaat niet!");
        }

        if (returnvalue.equals(password)) {
            label.setForeground(Color.blue);
            label.setText("Uw wachtwoord is correct");
            functionId = returnuser.getFunctionId();
            int userId = returnuser.getEmployeeId();
            scheduleManager.startDashboardGui(functionId,userId, employeeManager, availabilityManager, scheduleManager);
            frame.dispose();
        }

        if (!returnvalue.equals(password) && !returnvalue.equals("no_user")) {
            label.setText("Uw wachtwoord is verkeerd");
        }
    }

    public void startGui() {
        SignIn_GUI startgui = new SignIn_GUI(this);
    }

}
