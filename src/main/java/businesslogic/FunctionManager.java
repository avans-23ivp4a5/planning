/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import datastorage.FunctionDAO;
import domain.Employee;
import java.util.ArrayList;
import domain.Function;

/**
 *
 * @author Chiel
 */
public class FunctionManager {

    private ArrayList<Function> functions;

    public FunctionManager() {
        loadFunctions();
    }

    public void loadFunctions() {
        FunctionDAO functionDAO = new FunctionDAO();
        functions = functionDAO.findFunctions();
    }

    public ArrayList<Function> getFunctions() {
        if (functions.isEmpty()) {
            loadFunctions();
        }

        return functions;
    }
}
