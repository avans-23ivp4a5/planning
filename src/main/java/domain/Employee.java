/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Jari
 */
public class Employee {

    private int employeeId;
    private String username;
    private String password;
    private String firstName;
    private String infixName;
    private String lastName;
    private String dateOfBirth;
    private String accountNumber;
    private int hourlyWage;
    private int monthlyHours;
    private String address;
    private String postalCode;
    private String email;
    private String phoneNo;
    private String joinDate;
    private boolean active;
    private int functionId;
    private boolean isScheduled = false;
    
    private ArrayList<Availability> availabilityList;

    public Employee(int employeeId, String username, String password, String firstName, String infixName, String lastName, String dateOfBirth, String accountNumber, int hourlyWage, int monthlyHours, String address, String postalCode, String email, String phoneNo, String joinDate, boolean active, int functionId) {
        this.employeeId = employeeId;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.infixName = infixName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.accountNumber = accountNumber;
        this.hourlyWage = hourlyWage;
        this.monthlyHours = monthlyHours;
        this.address = address;
        this.postalCode = postalCode;
        this.email = email;
        this.phoneNo = phoneNo;
        this.joinDate = joinDate;
        this.active = active;
        this.functionId = functionId;
        
        availabilityList = new ArrayList<>();
    }
    
    public Employee(int employeeId, String firstName, String infixName, String lastName, int functionId) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.infixName = infixName;
        this.lastName = lastName;
        this.functionId = functionId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getInfixName() {
        return infixName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getHourlyWage() {
        return hourlyWage;
    }

    public int getMonthlyHours() {
        return monthlyHours;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public boolean isActive() {
        return active;
    }

    public int getFunctionId() {
        return functionId;
    }
    
    public String getFullName() {
        ArrayList<String> tmpEmployeeArray = new ArrayList<>();
        tmpEmployeeArray.add(getFirstName());
        tmpEmployeeArray.add(getInfixName());
        tmpEmployeeArray.add(getLastName());
        
        return tmpEmployeeArray.stream().filter(Objects::nonNull).collect(Collectors.joining(" "));
    }
    
    public boolean isScheduled() {
        return isScheduled;
    }
    
    public void setIsScheduled() {
        isScheduled = true;
    }

}
