/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Chiel
 */
public class Function {

    private int functionId;
    private String functionName;

    public Function(int functionId, String functionName) {
        this.functionId = functionId;
        this.functionName = functionName;
    }
    
    public int getFunctionId(){
        return functionId;
    }
    
    public String getFunctionName(){
        return functionName;
    }

}
