    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/*
 *
 * @author Jari
 */
public class Availability {
    private String date;
    private String shift;
    private int employeeId;
    private String checkInTime;
    private String checkOutTime;
    private boolean scheduled;

    public Availability(String date, String shift, int employeeId, String checkInTime, String checkOutTime, boolean scheduled) {
        this.date = date;
        this.shift = shift;
        this.employeeId = employeeId;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
        this.scheduled = scheduled;
    }

    public String getDate() {
        return date;
    }

    public String getShift() {
        return shift;
    }

    public int getEmployeeId() {
        return employeeId;
    }


    public String getCheckInTime() {
        return checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public boolean isScheduled() {
        return scheduled;
    }
}
