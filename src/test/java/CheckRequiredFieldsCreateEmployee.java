/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import businesslogic.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robin
 */
public class CheckRequiredFieldsCreateEmployee {
    
    EmployeeManager employeeManager;
    
    public CheckRequiredFieldsCreateEmployee() {
        
    }
    
    @Before
    public void setUp() {
        employeeManager = new EmployeeManager(new FunctionManager());
    }

    @Test
    public void testRequiredFieldsOne() {
        assertEquals(true, employeeManager.checkInsert_Boolean("test", "test", "2016-06-17"));
    }

    @Test
    public void testRequiredFieldsTwo() {
        assertEquals(false, employeeManager.checkInsert_Boolean("", "test", "2016-06-17"));
    }

    @Test
    public void testRequiredFieldThree() {
        assertEquals(false, employeeManager.checkInsert_Boolean("test", "", "2016-06-17"));
    }

    @Test
    public void testRequiredFieldsFour() {
        assertEquals(false, employeeManager.checkInsert_Boolean("test", "test", ""));
    }
}
