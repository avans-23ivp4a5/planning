/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import businesslogic.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Robin
 */
public class HourlyWageTest {
    
    EmployeeManager employeeManager;
    
    public HourlyWageTest() {
    
    }
    
    @Before
    public void setUp() {
        employeeManager = new EmployeeManager(new FunctionManager());
    }

    @Test
    public void testHourlyWageOne() {
        assertEquals(20, employeeManager.validateInput(20));
    }

    @Test
    public void testHourlyWageTwo() {
        assertEquals(0, employeeManager.validateInput(250));
    }

    @Test
    public void testHourlyWageThree() {
        assertEquals(0, employeeManager.validateInput(-201));
    }
}
